<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('page.register');
    }

    public function regname(Request $request){
        
        $fname = $request->fname;
        $lname = $request->lname;        
        
        return view('page.welcome', compact('fname', 'lname'));
    }
}
