@extends('template.master')

@section('title')
Data Casting
@endsection

@section('content')

<h3>{{$cast->nama}}</h3>
<h5>{{$cast->umur}} Tahun</h5>
<p>{{$cast->bio}}</p>

@endsection