<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<h1> Buat Account Baru! </h1>
    <h2> Sign Up Form </h2>

    <form action="/welcome" method="post">
        @csrf
        <label for="fname">First name:</label><br>
        <input type="text" id="fname" name="fname"><br>
        <label for="lname">Last name:</label><br>
        <input type="text" id="lname" name="lname">
    

    <p> Gender: </p>

    
      <input type="radio" id="html" name="Gender" value="Male">
      <label for="Male">Male</label><br>
      <input type="radio" id="css" name="Gender" value="Female">
      <label for="Female">Female</label><br>
      <input type="radio" id="javascript" name="Gender" value="Other">
      <label for="Other">Other</label>
    

    <p>Nationality:</p>
    
      <label></label>
        <select id="Nationality" name="Nationality">
          <option value="Indonesia">Indonesia</option>
          <option value="Amerika">Amerika</option>
          <option value="Inggris">Inggris</option>
        </select>
    

    <p>Language Spoken:</p>
    
      <input type="checkbox" id="Bahasa Indonesia" name="Bahasa Indonesia">
      <label for="Bahasa Indonesia"> Bahasa Indonesia</label><br>
      <input type="checkbox" id="English" name="English">
      <label for="English"> English</label><br>
      <input type="checkbox" id="Other" name="Other">
      <label for="Other"> Other</label>
    

    <p>Bio:</p>
    
      <textarea name="" rows="10" cols="30"></textarea>
    <br>
    <a href="/welcome"><input type="submit" value="Sign Up"></a>
    </form>
</body>
</html>